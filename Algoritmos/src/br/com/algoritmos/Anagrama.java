package br.com.algoritmos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class Anagrama {

	private LinkedHashMap<String, List<String>> anagramas;

	public Collection<List<String>> verificar(Collection<String> palavras){

		this.anagramas = new LinkedHashMap<String, List<String>>();

		for (String palavra : palavras) {

			String palavraOrdenada = ordenarPalavra(palavra);

			this.agruparAnagrama(palavra, palavraOrdenada);
		}

		return anagramas.values();
	}

	private void agruparAnagrama(String palavra, String palavraOrdenada) {
		
		List<String> lista = new ArrayList<String>();

		palavraOrdenada = palavraOrdenada.toUpperCase();
		
		if(this.anagramas.containsKey(palavraOrdenada)){
			lista = this.anagramas.get(palavraOrdenada);
		} 
		
		lista.add(palavra);
		this.anagramas.put(palavraOrdenada, lista);
	}

	private String ordenarPalavra(String palavra) {
		
		char[] charArray = palavra.toCharArray();
		
		Arrays.sort(charArray);

		return String.valueOf(charArray);
	}
}
