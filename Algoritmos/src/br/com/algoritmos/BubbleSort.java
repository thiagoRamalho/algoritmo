package br.com.algoritmos;

import java.util.Arrays;


public class BubbleSort {

	public String[] executar(String[] lista){
		
		int indiceVerificacao = lista.length;
		
		for (int i = 0; i < indiceVerificacao; i++) {
			
			System.out.println(i + " | "+Arrays.toString(lista));
			
			int indiceProximo = 1+i;
			
			//para evitar nullPointer
			indiceProximo = indiceProximo > lista.length-1 ? --indiceProximo : indiceProximo;
			
			//pega os primeiros pares para comparacao
			String a = lista[i];
			String b = lista[indiceProximo];
			
			if(a.compareTo(b) > 0){
				//se a maior que b permuta no array
				lista[i] = b;
				lista[indiceProximo] = a;
			}
			
			//se rodou por todos os elementos do array reinicia
			//os indices
			if(i == indiceVerificacao-1){
				//decrementa pois nesse ponto sabemos que o elemento
				//que chegou ao fim esta ordenado e nao precisa
				//mais ser verificado
				indiceVerificacao--;
				
				//zera o indice para recomecar o laco, como o mesmo
				//sera incrementado ao final setamos -1
				i = -1;
				System.out.println("------------------");
			}
		}
		
		return lista;
	}
}
