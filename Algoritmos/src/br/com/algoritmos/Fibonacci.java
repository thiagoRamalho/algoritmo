package br.com.algoritmos;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
	
	public List<Integer> gerarSequenciaCom(int elementos, int inicio){

		List<Integer> lista = new ArrayList<Integer>();

		for(int i = 1; i <= elementos; i++){

			if(i < 3){
				lista.add(inicio);
			}
			else {
				int indice = lista.size()-1;
				int ultimo = lista.get(indice);
				int penultimo = lista.get(indice-1);
				
				lista.add(ultimo+penultimo);
			}
		}

		return lista;
	}
}
