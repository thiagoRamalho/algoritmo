package br.com.algoritmos;

import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;

import br.com.algoritmos.Fibonacci;

public class FibonacciTest {


	@Test
	public void deveGerarSequenciaIniciandoEmUm() {

		List<Integer> sequencia = new Fibonacci().gerarSequenciaCom(11, 1);
		
	    assertThat(sequencia, Matchers.contains(1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89));
		
	}

	@Test
	public void deveGerarSequenciaIniciandoEm10() {

		List<Integer> sequencia = new Fibonacci().gerarSequenciaCom(4, 10);
		
	    assertThat(sequencia, Matchers.contains(10, 10, 20, 30));
	}
	
}
