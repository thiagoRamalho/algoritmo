package br.com.algoritmos;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class BubbleSortTest {
	
	private  final String[] LISTA_ORDENADA =  {"a","b","c","d","e"};

	@Test
	public void deveOrdenarTotalmenteDesordenada() {
		
		String[] lista = {"e","d","c","b","a"};
		
		lista = new BubbleSort().executar(lista);
		
		assertEquals(Arrays.asList(lista), Arrays.asList(LISTA_ORDENADA));
	}

	@Test
	public void deveOrdenarListaParcialMenteOrdenada() {
		
		String[] lista = {"c","d","e","b","a"};
		
		lista = new BubbleSort().executar(lista);
		
		assertEquals(Arrays.asList(LISTA_ORDENADA), Arrays.asList(lista));
	}

	@Test
	public void deveManterListaJaOrdenada() {
		
		String[] lista = new BubbleSort().executar(LISTA_ORDENADA.clone());
		
		assertEquals(Arrays.asList(LISTA_ORDENADA), Arrays.asList(lista));
   }

}
