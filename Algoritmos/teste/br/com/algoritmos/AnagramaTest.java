package br.com.algoritmos;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AnagramaTest {

	private Anagrama anagrama;

	@Before
	public void setUp() throws Exception {
		 anagrama = new Anagrama();
	}

	@Test
	public void deveAgruparPalavrasEmAnagramaEmApenasUmaLista() {
		
		List<String> entrada = Arrays.asList(new String[]{"cama", "maca", "mcaa"});
		
		Collection<List<String>> resultado = this.anagrama.verificar(entrada);
		
		assertEquals(1, resultado.size());
		
		Iterator<List<String>> iterator = resultado.iterator();
		
	    assertEquals(iterator.next(), Arrays.asList(new String[]{"cama", "maca", "mcaa"}));
	}

	@Test
	public void deveAgruparApenasPalavrasQueFormamAnagramas() {
		
		List<String> entrada = Arrays.asList(new String[]{"cama", "rota","MACA", "ator", "mcaa", "bota", "acma", "bata"});
		
		Collection<List<String>> resultado = this.anagrama.verificar(entrada);
		
		assertEquals(4, resultado.size());
		
		Iterator<List<String>> iterator = resultado.iterator();
		
	    assertEquals(iterator.next(), Arrays.asList(new String[]{"cama", "MACA", "mcaa", "acma"}));
	    assertEquals(iterator.next(), Arrays.asList(new String[]{"rota", "ator"}));
	    assertEquals(iterator.next(), Arrays.asList(new String[]{"bota"}));
	    assertEquals(iterator.next(), Arrays.asList(new String[]{"bata"}));
	}
}
